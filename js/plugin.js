/**
 * Get the current URL.
 *
 * @param {function(string)} callback called when the URL of the current tab is found.
 */
var currentTabURL = '';
function getCurrentTabUrl(callback) {
    // Query filter to be passed to chrome.tabs.query - see
    // https://developer.chrome.com/extensions/tabs#method-query
    var queryInfo = {
        active: true,
        currentWindow: true
    };

    chrome.tabs.query(queryInfo, function(tabs) {
        // chrome.tabs.query invokes the callback with a list of tabs that match the
        // query. When the popup is opened, there is certainly a window and at least
        // one tab, so we can safely assume that |tabs| is a non-empty array.
        // A window can only have one active tab at a time, so the array consists of
        // exactly one tab.
        var tab = tabs[0];

        // A tab is a plain object that provides information about the tab.
        // See https://developer.chrome.com/extensions/tabs#type-Tab
        var url = tab.url;

        // tab.url is only available if the "activeTab" permission is declared.
        // If you want to see the URL of other tabs (e.g. after removing active:true
        // from |queryInfo|), then the "tabs" permission is required to see their
        // "url" properties.
        console.assert(typeof url == 'string', 'tab.url should be a string');

        callback(url);
    });

    // Most methods of the Chrome extension APIs are asynchronous. This means that
    // you CANNOT do something like this:
    //
    // var url;
    // chrome.tabs.query(queryInfo, (tabs) => {
    //   url = tabs[0].url;
    // });
    // alert(url); // Shows "undefined", because chrome.tabs.query is async.
}

/**
 * Copy the chosen HTML banner markup to clipboard
 */
var copyMarkupToClipboard = function() {
    var bannerType = this.getAttribute("data-banner");
    var bannerMarkup = document.getElementById('markup_'+bannerType).value;

    /**
     * Replace URL with current tab URL
     */
    bannerMarkup = replaceFilmPageURL(bannerMarkup, bannerType);

    /**
     * Copy banner markup to clipboard
     * @param event
     */
    document.oncopy = function(event) {
        event.clipboardData.setData('text/plain', bannerMarkup);
        event.preventDefault();
    };
    document.execCommand("Copy", false, null);

    /**
     * Close the window
     */
    window.close();
};

/**
 * Replace default URL with current tab URL if from an ODEON site
 * @param bannerMarkup
 * @param bannerType
 * @returns string
 */
var replaceFilmPageURL = function(bannerMarkup, bannerType) {
    var check = new RegExp("odeon.co.uk|odeoncinemas.ie", "i");
    var checkResult = check.exec(currentTabURL);
    if (checkResult) {
         bannerMarkup = bannerMarkup.replace('http://www.odeon.url/', currentTabURL);
    }
    if (bannerType === 'mobile_leader') {
        bannerMarkup = bannerMarkup.replace('http://www.', 'http://mobi.');
    }
    return bannerMarkup;
};

/**
 * Run plugin on DOMContentLoaded event
 */
document.addEventListener('DOMContentLoaded', function () {
  var grabCodeButtons = document.getElementsByClassName('grab_code');
  for (var i = 0; i < grabCodeButtons.length; i++) {
      grabCodeButtons[i].addEventListener('click', copyMarkupToClipboard, false);
  }
  getCurrentTabUrl(function(url) {
      currentTabURL = url;
  });
});