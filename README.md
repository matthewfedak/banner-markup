# Banner Markup

#### What is it?

A simple Chrome Extension to generate HTML banner markup for ODEON.
Visit the film page on the ODEON Website that you wish to create a
banner for and click one of the 3 banner choices in the Chrome
 Extension. The associated banner markup will then be copied to the
 clipboard.

#### How to install

1. Clone the repo to your local machine.
2. Open Google Chrome and visit chrome://extensions
3. Enable the "Developer mode" checkbox at the top of the page
4. Click the "Load unpacked extension..." button and locate the
 "bannerMarkup" directory in the file prompt. Select the actual folder
 and not just the index.html file.
5. The extension should now be visible in the browser window. You
should see the extension icon which is a black letter "b".

#### How should I use?

Navigate to a film page on the odeon.co.uk or odeoncinemas.ie site and
click the Banner Markup extension icon. Click one of the buttons and
the relevant banner iFrame markup will be copied to the clipboard. The
 film page URL will be present in the markup.

#### FAQs

* What if I want to generate markup for a mobile banner but I am
viewing he desktop site?

    - A. No problem the extension will adjust the URL to point to the
mobile site for you.

* What if I am not viewing the ODEON website?

    -   The extension will still work but the markup will copied to the
    clipboard without the ODEON film page URL. You must then manually change
 the target URL in the markup later on.

* Why was this extension made?

    - To save a few seconds each weak when copying banner markup from
    the WIKI and then adding in the film page URL. It also helps
    prevent errors being made when manually updating the film page URL
    in the markup.

* Who made this?

    - Krankikom mitarbeiter [Matt Fedak](mailto:matthew.fedak@krankikom.de)
created this on a cold, wet and uneventful day in November 2017. It is
his first browser extension and hopefully not the last. If you have any
feature requests or ideas how to improve please let him know.

#### Contributing

If you would like to help in developing this extension, you can do so by
 cloning the repo and submitting a PR.